var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .addEntry('js/common', './web/js/common.js')
    .addEntry('components/weather/map', './web/components/weather/map/map.jsx')
    .addEntry('components/weather/history', './web/components/weather/history/history.jsx')
    .addEntry('components/weather/popup', './web/components/weather/popup/popup.jsx')
    .addEntry('components/weather/statistics', './web/components/weather/statistics/statistics.jsx')
    .addEntry('css/frontend/styles', './web/css/frontend/styles.scss')
    .enableSassLoader()
    .enableReactPreset();

module.exports = Encore.getWebpackConfig();