// set global variables for modules provided by webpack
const React = require('react');
const ReactDOM = require('react-dom');

global.React = React;
global.ReactDOM = ReactDOM;