import React from 'react';
import { Component } from 'react';
import { WeatherPopup } from '../popup/popup';
import GoogleMapReact from 'google-map-react';
import './map.scss';


const requestActualWeather = (latitiude, longtiude) => {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/weather/actual",
            data: {
                lat: latitiude,
                lon: longtiude
            }
        }).then(function (rawData){
            const res = {
                weather: rawData.weather,
                error: rawData.error
            };
            resolve(res);
        })
    })
};

export class WeatherMap extends Component {

    constructor(props) {
        super(props);
        this.state = {
            center: {
                lat: 52.2297,
                lng: 21.0122
            },
            zoom: 11,
            weather: null,
            loading: false,
            modal: false
        };

        this.clearWeather = this.clearWeather.bind(this);
        this.handleClickMap = this.handleClickMap.bind(this);
    }

    handleClickMap(x, y, lat, lng, event) {
        this.setState({
            loading: true
        });
        requestActualWeather(
            lat, lng
        ).then( res => {
            if (typeof res.weather !== 'undefined' && res.weather !== null) {
                this.setState({
                    loading: false,
                    modal: true,
                    weather: res.weather
                });
            } else {
                this.setState({
                    loading: false,
                    modal: false,
                    weather: null
                });
                alert(res.error);
            }
        });
    }

    clearWeather() {
        this.setState({
            weather: null
        });
    }

    renderLoading() {
        return (
            <div className="loading">
                <div className="spinner">
                    <h2>Saving data...</h2>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div>
                <h1>WeatherMap</h1>
                <div className="map">
                    <GoogleMapReact
                        bootstrapURLKeys={{ key: 'AIzaSyAzgRKKOEJiDIlNK55MYytBP7JsKmeLwu8' }}
                        defaultCenter={this.state.center}
                        defaultZoom={this.state.zoom}
                        onClick={({x, y, lat, lng, event}) => this.handleClickMap(x, y, lat, lng, event)}
                        className="map"
                    />
                </div>
                { this.state.loading && this.renderLoading()}
                { !this.state.loading && !!this.state.weather && <WeatherPopup clearWeather={this.clearWeather} weather={this.state.weather}/>}
            </div>
        );
    }
}

global.WeatherMap = WeatherMap;
