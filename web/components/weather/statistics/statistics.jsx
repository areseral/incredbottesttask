import React from 'react';
import { Component } from 'react';
import './statistics.scss';


const requestStatisticsData = (page) => {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/weather/history/statistics",
            data: {
                page: page
            }
        }).then(function (rawData){
            const res = {
                stats: rawData.stats,
                success: rawData.success
            };
            resolve(res);
        })
    })
};


export class WeatherStatistics extends Component {
    constructor(props) {
        super(props);
        this.state = {
            statistics: null,
            loading: true
        };
    }

    componentDidMount() {
        requestStatisticsData().then( res => {
            if (res.success) {
                this.setState({
                    statistics: res.stats,
                    loading: false
                });
            }
        });
    }

    render() {
        return (
            <div>
                { this.state.loading && <p>Loading...</p> }
                { this.state.statistics !== null && (
                    <div>
                        <p>Min Temp.: {this.state.statistics.minTemperature}</p>
                        <p>Max Temp.: {this.state.statistics.maxTemperature}</p>
                        <p>Average Temp.: {this.state.statistics.avgTemperature}</p>
                        <p>Most Search City: {this.state.statistics.mostSearchCity}</p>
                        <p>Count of Search: {this.state.statistics.sumOfSearch}</p>
                    </div>
                )}
            </div>
        );
    }
}

global.WeatherStatistics = WeatherStatistics;
