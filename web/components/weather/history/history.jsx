import React from 'react';
import { Component } from 'react';
import { WeatherStatistics } from "../statistics/statistics";
import './history.scss';

const requestHistoryData = (page) => {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/weather/history/data",
            data: {
                page: page
            }
        }).then(function (rawData){
            const res = {
                data: rawData.data
            };
            resolve(res);
        })
    })
};

export class WeatherHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            page: 0,
            pages: 0,
            pageSize: 0,
            loading: true
        };

        this.changePage = this.changePage.bind(this);
    }

    componentDidMount() {
        requestHistoryData(
            this.state.page
        ).then( res => {
            this.setState({
                data: res.data.results,
                pages: res.data.pages,
                pageSize: res.data.pageSize,
                loading: false
            })
        });
    }

    changePage(event) {
        var page = event.target.value;

        this.setState({
            page: page,
            loading: true
        });

        requestHistoryData(
            page
        ).then( res => {
            this.setState({
                data: res.data.results,
                pages: res.data.pages,
                pageSize: res.data.pageSize,
                loading: false
            })
        });

    }

    renderData() {
        if (this.state.loading) {
            return (<p>Loading...</p>);
        }

        if (!this.state.data) {
            return this.renderEmptyDataInfo();
        }

        return this.renderDataTable();
    }

    renderDataTable() {
        return (
            <div>
                <br/>
                <br/>
                <br/>
                <p>
                    Select page:
                    <select id="page" onChange={this.changePage.bind(this)} value={this.state.page}>
                        {[...Array(this.state.pages)].map((x, index) =>
                            <option key={index} value={index}>{index + 1}</option>
                        )}
                    </select>
                </p>
                <table>
                    <thead>
                        <tr>
                            <td>Id</td>
                            <td>Latitiude</td>
                            <td>Longitiude</td>
                            <td>City</td>
                            <td>Temp.</td>
                            <td>Clouds</td>
                            <td>Wind speed</td>
                            <td>Wind degree</td>
                            <td>Description</td>
                            <td>Search At</td>
                        </tr>
                    </thead>
                    <tbody>
                        { this.state.data.map((item, index) => (
                            <tr key={index}>
                                <td>{ item.id }</td>
                                <td>{ item.latitiude }</td>
                                <td>{ item.longtitiude }</td>
                                <td>{ item.city }</td>
                                <td>{ item.temperature }</td>
                                <td>{ item.clouds }</td>
                                <td>{ item.windSpeed }</td>
                                <td>{ item.windDegree }</td>
                                <td>{ item.description }</td>
                                <td>{ item.searchAt ? item.searchAt.date : '' }</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        );
    }

    renderEmptyDataInfo() {
        return (
            <div>
                Nothing to show ;(
            </div>
        )
    }

    render() {
        return (
            <div>
                <h1>Weather History</h1>
                <WeatherStatistics />
                { this.renderData() }
            </div>
        );
    }
}

global.WeatherHistory = WeatherHistory;
