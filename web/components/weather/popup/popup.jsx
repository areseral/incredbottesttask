import React from 'react';
import { Component } from 'react';
import './popup.scss';

export class WeatherPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            weather: props.weather
        };

        this.closePopup = this.closePopup.bind(this);
    }

    closePopup() {
        this.setState({
            weather: null
        });
        this.props.clearWeather();
    }

    render() {
        return (
            <div id="weather-popup">
                {this.state.weather !== null && (
                    <div className="overlay">
                        <div className="popup">
                            <h2 onClick={this.closePopup} style={{float: 'right', cursor: 'pointer'}}>
                                X
                            </h2>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>City</td>
                                        <td>{this.state.weather.city}</td>
                                    </tr>
                                    <tr>
                                        <td>Clouds</td>
                                        <td>{this.state.weather.clouds}</td>
                                    </tr>
                                    <tr>
                                        <td>Temperature</td>
                                        <td>{this.state.weather.temperature}</td>
                                    </tr>
                                    <tr>
                                        <td>Wind Degree</td>
                                        <td>{this.state.weather.wind_degree}</td>
                                    </tr>
                                    <tr>
                                        <td>Wind Speed</td>
                                        <td>{this.state.weather.wind_speed}</td>
                                    </tr>
                                    <tr>
                                        <td>Description</td>
                                        <td>{this.state.weather.description}</td>
                                    </tr>
                                    <tr>
                                        <td>Search Time</td>
                                        <td>{this.state.weather.dt}</td>
                                    </tr>
                                    <tr>
                                        <td>Latitiude</td>
                                        <td>{this.state.weather.latitiude}</td>
                                    </tr>
                                    <tr>
                                        <td>Longitiude</td>
                                        <td>{this.state.weather.longtitiude}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

global.WeatherPopup = WeatherPopup;
