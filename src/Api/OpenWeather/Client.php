<?php

namespace App\Api\OpenWeather;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;
use GuzzleHttp;

class Client
{
    const PARAM_LAT = 'lat';
    const PARAM_LON = 'lon';
    const PARAM_UNITS = 'units';
    const PARAM_APPID = 'APPID';
    const WEATHER_URI = 'weather';
    const REQUEST_TYPE_GET = 'GET';
    const REQUEST_TYPE_POST = 'POST';

    /**
     * @var GuzzleHttp\Client
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * Client constructor.
     * @param LoggerInterface $logger
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(LoggerInterface $logger, ParameterBagInterface $parameterBag)
    {
        $this->client = new GuzzleHttp\Client();
        $this->logger = $logger;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @param $lat
     * @param $lon
     * @return mixed|null
     * @throws GuzzleHttp\Exception\GuzzleException
     */
    public function getActualWeather($lat, $lon)
    {
        try {
            $response = $this->client->request(
                self::REQUEST_TYPE_GET,
                $this->parameterBag->get('openweather')['api_url'] . '/' . self::WEATHER_URI,
                ['query' => [
                    self::PARAM_APPID => $this->parameterBag->get('openweather')['api_key'],
                    self::PARAM_UNITS => $this->parameterBag->get('openweather')['units'],
                    self::PARAM_LAT => $lat,
                    self::PARAM_LON => $lon
                ]]
            );

            if ($response->getStatusCode() !== 200) {
                $this->logger->error('OpenWeatherClient Error: Problem with response code: ' . $response->getStatusCode());
                return null;
            }

            $responseData = json_decode($response->getBody()->getContents(), true);
            if ($this->validateWeatherData($responseData)) {
                return $this->prepareData($responseData);
            }

            return null;
        } catch (\Exception $e) {
            $this->logger->error('OpenWeatherClient Error: ' . $e->getMessage());
            return null;
        }
    }

    /**
     * @param $weather
     * @return array
     */
    private function prepareData($weather)
    {
        return [
            'city' => $weather['name'],
            'clouds' => $weather['clouds']['all'],
            'description' => $weather['weather'][0]['description'],
            'latitiude' => $weather['coord']['lat'],
            'longtitiude' => $weather['coord']['lon'],
            'temperature' => $weather['main']['temp'],
            'wind_degree' => $weather['wind']['deg'],
            'wind_speed' => $weather['wind']['speed'],
            'dt' => $weather['dt']
        ];
    }

    /**
     * @param $responseData
     * @return bool
     */
    private function validateWeatherData($responseData)
    {
        return array_key_exists('name', $responseData) &&
            array_key_exists('clouds', $responseData) &&
            array_key_exists('weather', $responseData) &&
            array_key_exists('main', $responseData) &&
            array_key_exists('wind', $responseData);
    }
}