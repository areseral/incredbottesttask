<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WeatherHistoryRepository")
 */
class WeatherHistory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=6)
     */
    private $latitiude;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=6)
     */
    private $longtitiude;

    /**
     * @ORM\Column(type="string")
     */
    private $city;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=3)
     */
    private $temperature;

    /**
     * @ORM\Column(type="integer")
     */
    private $clouds;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2)
     */
    private $windSpeed;

    /**
     * @ORM\Column(type="integer")
     */
    private $windDegree;

    /**
     * @ORM\Column(type="string")
     */
    private $description;

    /**
     * @ORM\Column(name="search_at", type="datetime", nullable=true)
     */
    private $searchAt = null;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt = null;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt= new \DateTime();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLatitiude()
    {
        return $this->latitiude;
    }

    public function setLatitiude($latitiude): self
    {
        $this->latitiude = $latitiude;

        return $this;
    }

    public function getLongtitiude()
    {
        return $this->longtitiude;
    }

    public function setLongtitiude($longtitiude): self
    {
        $this->longtitiude = $longtitiude;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getTemperature()
    {
        return $this->temperature;
    }

    public function setTemperature($temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getClouds(): ?int
    {
        return $this->clouds;
    }

    public function setClouds(int $clouds): self
    {
        $this->clouds = $clouds;

        return $this;
    }

    public function getWindSpeed()
    {
        return $this->windSpeed;
    }

    public function setWindSpeed($windSpeed): self
    {
        $this->windSpeed = $windSpeed;

        return $this;
    }

    public function getWindDegree(): ?int
    {
        return $this->windDegree;
    }

    public function setWindDegree(int $windDegree): self
    {
        $this->windDegree = $windDegree;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSearchAt(): ?\DateTimeInterface
    {
        return $this->searchAt;
    }

    public function setSearchAt(?\DateTimeInterface $searchAt): self
    {
        $this->searchAt = $searchAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

}
