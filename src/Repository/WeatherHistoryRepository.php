<?php

namespace App\Repository;

use App\Entity\WeatherHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WeatherHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method WeatherHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method WeatherHistory[]    findAll()
 * @method WeatherHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeatherHistoryRepository extends ServiceEntityRepository
{
    const SIZE_PAGE_HISTORY = 10;

    /**
     * WeatherHistoryRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WeatherHistory::class);
    }

    /**
     * @param $page
     * @param int $pageSize
     * @return array
     */
    public function fetchHistoryData($page, $pageSize = self::SIZE_PAGE_HISTORY)
    {
        $query = $this->createQueryBuilder('wh')
            ->orderBy('wh.searchAt', 'DESC');

        $maxResults = count($query->getQuery()->getArrayResult());

        $query->setFirstResult($page * $pageSize)
            ->setMaxResults($pageSize);

        $results = $query->getQuery()->getArrayResult();
        $pages = (int) ceil($maxResults / $pageSize);

        return [
            'results' => $results,
            'pages' => $pages,
            'pageSize' => $pageSize
        ];
    }

    /**
     * @return array
     */
    public function fetchStatisticsData()
    {
        return [
            'minTemperature' => $this->getMinTemperature(),
            'maxTemperature' => $this->getMaxTemperature(),
            'avgTemperature' => $this->getAverageTemperature(),
            'mostSearchCity' => $this->getMostSearchCity(),
            'sumOfSearch'   => $this->getSumOfSearchHistory()
        ];
    }

    /**
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMinTemperature()
    {
        $result = $this->createQueryBuilder('wh')
            ->select('MIN(wh.temperature) as min')
            ->getQuery()
            ->getOneOrNullResult();

        return $result !== null ? $result['min'] : '';
    }

    /**
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMaxTemperature()
    {
        $result = $this->createQueryBuilder('wh')
            ->select('MAX(wh.temperature) as max')
            ->getQuery()
            ->getOneOrNullResult();

        return $result !== null ? $result['max'] : '';
    }

    /**
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAverageTemperature()
    {
        $result = $this->createQueryBuilder('wh')
            ->select('AVG(wh.temperature) as avg')
            ->getQuery()
            ->getOneOrNullResult();

        return $result !== null ? $result['avg'] : '';
    }

    /**
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMostSearchCity()
    {
        $result = $this->createQueryBuilder('wh')
            ->select('wh.city')
            ->addSelect('count(wh.city) as cityCount')
            ->groupBy('wh.city')
            ->orderBy('cityCount', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        return $result !== null ? $result['city'] : '';
    }

    /**
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getSumOfSearchHistory()
    {
        $result = $this->createQueryBuilder('wh')
            ->select('count(1) as count')
            ->getQuery()
            ->getOneOrNullResult();

        return $result !== null ? $result['count'] : '';
    }
}
