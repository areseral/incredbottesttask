<?php

namespace App\Controller;

use App\Entity\WeatherHistory;
use App\Api\OpenWeather\Client as OpenWeatherApiCLient;
use Doctrine\ORM\Query\Parameter;
use PhpParser\Node\Param;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class WeatherController extends AbstractController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * WeatherController constructor.
     * @param LoggerInterface $logger
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(LoggerInterface $logger, ParameterBagInterface $parameterBag) {
        $this->logger = $logger;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @Route("/weather", name="weather")
     */
    public function index()
    {
        return $this->render('weather/map.html.twig');
    }

    /**
     * @Route("/weather/history", name="weather_history")
     * @Method("GET")
     */
    public function history()
    {
        return $this->render('weather/history.html.twig');
    }

    /**
     * @Route("/weather/history/data", name="weather_history_data")
     * @Method("GET")
     */
    public function historyData(Request $request)
    {
        $historyRepository = $this->getDoctrine()->getRepository(WeatherHistory::class);

        try {
            $history = $historyRepository->fetchHistoryData($request->request->get('page'));

            return new JsonResponse([
                'data' => $history
            ]);
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false, 'error' => $e->getMessage()]);
        }
    }

    /**
     * @Route("/weather/history/statistics", name="weather_history_statistics")
     * @Method("POST")
     */
    public function historyStatistics()
    {
        $historyRepository = $this->getDoctrine()->getRepository(WeatherHistory::class);

        try {
            $stats = $historyRepository->fetchStatisticsData();

            return new JsonResponse([
                'stats' => $stats,
                'success' => true
            ]);
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false, 'error' => $e->getMessage()]);
        }
    }

    /**
     * @Route("/weather/actual", name="weather_actual")
     * @Method("POST")
     */
    public function actual(Request $request)
    {
        try {
            $weatherClient = new OpenWeatherApiCLient($this->logger, $this->parameterBag);
            $weatherData = $weatherClient->getActualWeather(
                $request->request->get('lat'),
                $request->request->get('lon')
            );

            if (!$weatherData) {
                return new JsonResponse(['success' => false, 'error' => 'No weather for this location ;(']);
            }

            $entityManager = $this->getDoctrine()->getManager();

            $weatherHistory = new WeatherHistory();
            $weatherHistory->setCity($weatherData['city']);
            $weatherHistory->setClouds($weatherData['clouds']);
            $weatherHistory->setDescription($weatherData['description']);
            $weatherHistory->setLatitiude($weatherData['latitiude']);
            $weatherHistory->setLongtitiude($weatherData['longtitiude']);
            $weatherHistory->setTemperature($weatherData['temperature']);
            $weatherHistory->setWindDegree($weatherData['wind_degree']);
            $weatherHistory->setWindSpeed($weatherData['wind_speed']);

            $dt = new \DateTime();
            $dt->setTimestamp($weatherData['dt']);
            $weatherData['dt'] = $dt->format('Y-m-d H:i:s');

            $weatherHistory->setSearchAt($dt);

            $entityManager->persist($weatherHistory);
            $entityManager->flush();

            return new JsonResponse([
                'error' => false,
                'weather' => $weatherData
            ]);
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false, 'error' => $e->getMessage()]);
        }
    }
}